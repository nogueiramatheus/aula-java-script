// Apaga Livros já adicionados
const elementsList = document.querySelector("#elements")
elementsList.addEventListener("click", (e) => {
    if (e.target.classList == "delete")
        elementsList.removeChild(e.target.parentElement);
})

//Adiciona livro de acordo com o texto do input
const bookButton = document.querySelector("#add-book button")
const bookText = document.querySelector("#add-book input")
bookButton.addEventListener("click", (e) => {
        novoLivro = document.createElement('li');
        novoLivroNome = document.createElement('span');
        novoLivroNome.innerText = bookText.value;
        bookText.value = "";
        novoLivroNome.classList.add("name")
        novoLivrobutton = document.createElement('span');
        novoLivrobutton.innerText = "excluir";
        novoLivrobutton.classList.add("delete")
        novoLivro.appendChild(novoLivroNome)
        novoLivro.appendChild(novoLivrobutton)
        elementsList.appendChild(novoLivro)
})

// Previne a página de recarregar com o click no form
const btun = document.querySelector("#add-book");
btun.addEventListener("click", (e) => {
    e.preventDefault();
})